# bot_remover

Adds a small code to .htaccess file to disable unnecessary robots, spiders and crawlers from visiting and crawling your website

The file bot_remover.txt contains the mod_rewrite rules for .htaccess file of website public directory.

You should replace https://example.com/ with your website name.

The robots of google, bing, duckduckgo and brave are allowed (and others like applebot can also be added).

If you want to allow other types of robots, spiders and crawlers, include their name in the list of (google|bing|duckduckgo|brave) like (google|applebot|bing|duckduckgo|brave).

The remaining bots, spiders and crawlers are rejected with a 405 HTTP code, you can change that to whatever HTTP code you want.